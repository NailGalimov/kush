// Проверка на ios
var iOS = /iPad|iPhone|iPod/.test(navigator.platform || "");
if (iOS) {
	$('body').addClass('ios');
}

function setViewport() {
    var mvp = $('#myViewport');
    if (window.matchMedia("(max-width: 360px)").matches) {
        mvp.attr('content', 'user-scalable=no, width=360');
    } else {
        mvp.attr('content', 'width=device-width');
    }
};

$(window).resize(function () {
    setViewport()
});


$(function () {
	setViewport();

	var myLazyLoad = new LazyLoad();

	myLazyLoad.update();



	$(".our-cases__slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: ".our-cases__dots",
		arrows: true,
		appendArrows: ".our-cases__arrows",
		prevArrow: "<span class='icon-left-arrow'></span>",
		nextArrow: "<span class='icon-right-arrow'></span>"

	});
	$(".our_team__container").on('init', function(slick){
		var active = $('.our_team__container .slick-active');
		var last = active.eq( active.length - 1 );
		last.addClass('last');
	});
	$(".our_team__container").slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		appendArrows: ".our_team__arrows",
		prevArrow: "<span class='icon-left-arrow'></span>",
		nextArrow: "<span class='icon-right-arrow'></span>",
		responsive: [
			{
			breakpoint: 1367,
				settings: {
					autoplay: true,
					autoplaySpeed: 2000,
					pauseOnFocus: false,
					pauseOnHover: false,
					speed: 900
				}
			},
			{
			breakpoint: 1025,
				settings: {
					slidesToShow: 3,
					autoplay: true,
					autoplaySpeed: 1700,
					pauseOnFocus: false,
					pauseOnHover: false,
					speed: 900
				}
			},
			{
			breakpoint: 740,
				settings: {
					slidesToShow: 2,
					autoplay: true,
					autoplaySpeed: 2000,
					pauseOnFocus: false,
					pauseOnHover: false,
					speed: 900
				}
			},
			{
			breakpoint: 376,
				settings: {
					slidesToShow: 1,
					autoplay: true,
					autoplaySpeed: 2000,
					pauseOnFocus: false,
					pauseOnHover: false,
					speed: 900
				}
			},
		]
	})

	$(".our-cases__gallery").slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 100,
		pauseOnFocus: false,
		pauseOnHover: false,
		speed: 30000,
		responsive: [
			{
			breakpoint: 1537,
				settings: {
					slidesToShow: 4,
				}
			},
			{
			breakpoint: 1025,
				settings: {
					slidesToShow: 3,
				}
			},
			{
			breakpoint: 770,
				settings: {
					slidesToShow: 2,
				}
			},
		]

	});

	$('.our-cases__slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
		var slider = $(this);

		slider.find(".cases_item__info").removeClass("active");
		slider.find(".show_more").removeClass("active");

	});

	$('.our_team__container').on('afterChange', function(event, slick, currentSlide, nextSlide){
		var active = $('.our_team__container .slick-active');
		var last = active.eq( active.length - 1 );
		active.removeClass('last')
		last.addClass('last');
	});


	// главный экран анимация
	var tl = gsap.timeline();

	$(document).on("click", ".hero-custom__shows.hero__shows li", function(e){
		var th = $(this);
		var parent = th.closest("ul");
		var dataId = th.attr("data-id");
		parent.attr("data-showed-item", dataId);
		var showItemAttr = parent.attr("data-showed-item");

		console.log(showItemAttr);

		gsap.to(".hero-text__inner", {
			opacity: 0,
			y: 150,
			visibility: "hidden",
			duration: .3
		})
		gsap.to(".hero__shows-visible-text", {
			opacity: 0,
			y: 150
		})

		gsap.to(".hero-custom--close-shows", {
			y: 0,
			opacity: 1,
			duration: .3
		})

		gsap.fromTo(th.find(".hero__shows--item_info"), {
			y: 100,
			opacity: 0
		},
		{
			y: 0,
			opacity: 1,
			duration: .4,
			delay: .5
		})

	});

	$(document).on("click", ".hero-custom--close-shows", function(){
		var th = $(this);
		var parent = th.closest(".hero-custom");
		var showItemAttr = parent.find(".hero__shows").attr("data-showed-item");

		parent.find(".hero__shows").removeAttr("data-showed-item");

		tl.to(".hero-text__inner", {
			opacity: 1,
			y: 0,
			visibility: "visible",
			duration: .3
		},"+=0.5").to(".hero__shows-visible-text", {
			opacity: 1,
			y: 0
		}, "+=0.2");

		gsap.to(".hero-custom--close-shows", {
			y: 150,
			opacity: 0,
			duration: .3
		})

		gsap.to(".hero__shows--item_info", {
			y: 100,
			opacity: 0,
			duration: .3
		})

		switch(showItemAttr) {
			case '1':
				gsap.to(parent.find(["data-id=1"]).find(".hero__shows--item_info"), {
					y: 150,
					opacity: 0,
					duration: .3
				})
			  break
			case '2':
				gsap.to(parent.find(["data-id=2"]).find(".hero__shows--item_info"), {
					y: 150,
					opacity: 0,
					duration: .3
				})
			break
			case '3':
				gsap.to(parent.find(["data-id=3"]).find(".hero__shows--item_info"), {
					y: 150,
					opacity: 0,
					duration: .3
				})
			break
		}
	});

	let timeline = gsap.timeline();

	// ScrollTrigger.create({
	// 	trigger: ".hero-custom",
	// 	animation: timeline,
	// 	scrub: true,
	// 	pin: true,
	// 	start: "top top"
	// });


	timeline.to(".about_us", {
		// x: 0,
		y: 0,
		duration: 4,
		scrollTrigger: {
			trigger: '.hero-custom',
			scrub: true,
			pin: true,
			scrub: 1,
			start: "top top"
		}
	});

	// фиксируем хэдер при скролле
	const header = $(".header-custom");
	const firstBlock = $(".hero-custom");
	const headerHeight = header.outerHeight();
	const firstBlockHeight = firstBlock.outerHeight();

	$(document).on("scroll", function() {
		var scrollDistance = window.scrollY;

		if (scrollDistance >= headerHeight) {
			header.css("opacity", "0");
		} else {
			header.css("opacity", "1");
		}

		if (scrollDistance > firstBlockHeight) {
			header.addClass("header--fixed");
			header.css("opacity", "1");
		} else {
			header.removeClass("header--fixed");

		}
	});


	// открытие меню в мобилке
	let newTl = gsap.timeline({
		paused: true,
		reversed: true
	})

	newTl
		.set(".menu__mobile", {top: 0})
		.to(".menu__mobile", {
			duration: 0.7,
			width: "100%",
			ease: Power4.easeInOut,
		})
		.from(".menu__mobile .main-nav ul li", {
			duration: 0.4,
			opacity: 0,
			x: 25
		})
		.from(".menu__mobile .main-links a", {
			duration: 0.4,
			opacity: 0,
			y: 25
		}, "-=0.3")
		.from(".menu__mobile .main-location", {
			duration: 0.4,
			opacity: 0,
			y: 25
		},"-=0.3")
		.from(".menu__mobile .main-tel", {
			duration: 0.4,
			opacity: 0,
			y: 25
		}, "-=0.3")


	$(document).on("click", ".burger-button", function(e){

		$(this).toggleClass("active");

		if (newTl.isActive()) {
			e.preventDefault();
			return false;
		}

		isReversed(newTl);
	});

	function isReversed(animation) {
		animation.reversed() ? animation.play() : animation.reverse();
	}

	// показать еще
	$(document).on("click", ".show_more", function(){
		var parent = $(this).closest(".cases_item__info");

		$(this).toggleClass("active");

		parent.toggleClass("active");


	});




	// фильтрайия ввода
	$("[data-mask=numbers]").formatFeild({
		integerPositive: true,
		lettersOnly: false,
	});
	$("[data-mask=letter]").formatFeild({
		lettersOnly: true,
		layout: "toRu"

	});
	$("[data-mask=calendar]").mask("99.99.9999");


	$.validator.addMethod(
		"calendar",
		function (value, element) {
			return /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(
				value
			);
		},
		"Неккоректная дата"
	);

	$("[data-mask=calendar]").rules("add", {
		calendar: true,
	});

});
