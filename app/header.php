<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Кушавель</title>

	<!-- <link rel="apple-touch-icon" sizes="120x120" href="include/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="include/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="include/favicon/favicon-16x16.png">
	<link rel="manifest" href="include/favicon/site.webmanifest">
	<link rel="mask-icon" href="include/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff"> -->

	<link rel="stylesheet" href="assets/css/libs.style.css">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="assets/css/base.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="body-custom">

	<header class="header-custom">
		<div class="header-container">
				<div class="header-custom__inner">
					<a href="#" class="main-logo">
						<img src="./assets/images/logo.png" alt="Логотип">
						<p>Event-агентство полного цикла</p>
					</a>


				<nav class="header-custom__nav main-nav">
					<ul>
						<li><a href="#">О нас</a></li>
						<li><a href="#">Услуги</a></li>
						<li><a href="#">Проекты</a></li>
						<li><a href="#">Контакты</a></li>
					</ul>
				</nav>

				<div class="header-custom__location main-location">
					<p>
						г. Санкт-Петербург
						Ленинградская область
					</p>
				</div>

				<div class="header-custom__links main-links">
					<a class="main-links__icon" href="#"><span class="icon-fb"></span></a>
					<a class="main-links__icon" href="#"><span class="icon-inst"></span></a>
					<a class="main-links__icon" href="#"><span class="icon-triangle"></span></a>
				</div>

				<div class="header-custom__tel main-tel">
					<a href="tel:+78129211152">+ 7 (812) 92-111-52</a>
				</div>
			</div>
			<div class="main-btn-link">
				<button>
					<span data-text="Оставить заявку">Оставить заявку</span>
				</button>
			</div>
		</div>

	</header>

	<main>

