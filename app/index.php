<? include $_SERVER['DOCUMENT_ROOT'].'/app/header.php'?>

<button class="burger-button"></button>


<div class="menu__mobile">
	<div class="menu__mobile_inner">
		<nav class="main-nav">
			<ul>
				<li><a href="#">О нас</a></li>
				<li><a href="#">Услуги</a></li>
				<li><a href="#">Проекты</a></li>
				<li><a href="#">Контакты</a></li>
			</ul>
		</nav>



		<div class="main-links">
			<a class="main-links__icon" href="#"><span class="icon-fb"></span></a>
			<a class="main-links__icon" href="#"><span class="icon-inst"></span></a>
			<a class="main-links__icon" href="#"><span class="icon-triangle"></span></a>
		</div>

		<div class="main-location">
			<p>
				г. Санкт-Петербург
				Ленинградская область
			</p>
		</div>

		<div class="main-tel">
			<a href="tel:+78129211152">+ 7 (812) 92-111-52</a>
		</div>
	</div>
</div>

<section class="section hero-custom">

	<div class="scroll-icon"><span class="icon-mouse"></span></div>


	<div class="hero-custom__info hero-text">
		<div class="hero-text__inner">
			<h1>
				Продюсируем главные события <span>вашей жизни за 2 дня</span>
			</h1>
			<p>Мы сделаем из вашей истории живую сенсацию с безупречным вкусом</p>
		</div>
	</div>

	<ul class="hero-custom__shows hero__shows">
		<li data-id="1" >
			<div class="hero__shows_item_bg" style="background-image: url(./assets/images/shows_img1.jpg)"></div>
			<span class="hero__shows-visible-text">Организуем мероприятия</span>
			<i class="hero__shows-hidden-text">Меропритяия</i>

			<div class="hero__shows--item_info">
				<h3>Организуем мероприятия</h3>
				<p>Устроим событие в музее или дворце и еще 100+ пространств по Петербургу</p>
			</div>
		</li>
		<li data-id="2">
			<div class="hero__shows_item_bg" style="background-image: url(./assets/images/shows_img2.jpg)"></div>
			<span class="hero__shows-visible-text">Проводим праздники</span>
			<i class="hero__shows-hidden-text">Праздники</i>

			<div class="hero__shows--item_info">
				<h3>Проводим праздники</h3>
				<p>Режиссируем мероприятия на 5 000+ человек</p>
			</div>
		</li>
		<li data-id="3">
			<div class="hero__shows_item_bg" style="background-image: url(./assets/images/shows_img3.jpg)"></div>
			<span class="hero__shows-visible-text">Воплощаем мечты</span>
			<i class="hero__shows-hidden-text">Мечты</i>

			<div class="hero__shows--item_info">
				<h3>Воплощаем мечты</h3>
				<p>Превратим вашу идею в мероприятие с вау-эффектом</p>
			</div>
		</li>
	</ul>

	<button class="hero-custom--close-shows">
		<span class="icon-close"></span>
    </button>


	<div class="section section about_us">
		<div class="container-fluid">
			<div class="about_us__row">
				<div class="about_us__col">
					<h2>
						Делаем события <span>легендарными</span>
					</h2>

					<div class="about_us__text">
						<div class="about_us-rotate_text">
							<p>
								<strong>Мы миксуем коктейль</strong>
								из впечатлений, вдохновения
								и восторга
							</p>
						</div>
						<div class="about_us-normal_text">
							<p>
								<strong>Мы – креативная команда из 50 мастеров,</strong>
								и для нас нет ничего невозможного. Режиссируем исключительные мероприятия: будь то свадьба <br>
								на 300 персон или тимбилдинг на природе.
							</p>
							<p>
								<strong>Берем на себя весь процесс: от шоу-программы до авторской кухни.</strong>
								Организуем выездной кейтеринг <br>
								и корпоративное питание для усиления узнаваемости вашего бренда. Что бы вы ни задумали, наш 10-летний опыт и прогрессивный подход гарантируют
								сенсационный результат.
							</p>
						</div>
					</div>
				</div>

				<div class="about_us__col">
					<div class="about_us__img">

					<picture>
						<source data-srcset="./assets/images/webp/about_us_img.webp" type="image/webp">
						<source data-srcset="./assets/images/about_us_img.png" type="image/png">
						<img class="lazy" data-src="./assets/images/about_us_img.png" alt="Наша команда">
					</picture>



					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section history">
	<div class="history__img">
		<img src="./assets/images/fork.png" alt="">
	</div>
	<div class="container-fluid">
		<div class="history__title">
			<h2>
				Мы провели мероприятий
				<span>на 4 000 000 рублей за 1 неделю</span>
			</h2>
			<p>
				И точно знаем, как объединить и удивить
				до 5 000 гостей под одной крышей
				или под открытым небом
			</p>
		</div>

		<div class="history__list">
			<div class="history__col">
				<div class="history__item">
					<div class="history__item-title">
						<h4>
							10 000
						</h4>
						<p>
							реализованных проектов
						</p>
					</div>
					<p>
						по всей России и СНГ
					</p>
				</div>
			</div>
			<div class="history__col">
				<div class="history__item">
					<div class="history__item-title">
						<h4>
							10 000 м²
						</h4>
						<p>
							Собственное производство
						</p>
					</div>
					<p>
						Кухня и арт-пространство
					</p>
				</div>
			</div>
			<div class="history__col">
				<div class="history__item">
					<div class="history__item-title">
						<h4>
							150+
						</h4>
						<p>
							ключевых, постоянных клиентов
						</p>
					</div>
					<p>
						работают с нами – от 5 лет
					</p>
				</div>
			</div>
			<div class="history__col">
				<div class="history__item">
					<div class="history__item-title">
						<h4>
							1 500+
						</h4>
						<p>
							блюд высокой кухни
						</p>
					</div>
					<p>
						под любой еvent-формат
					</p>
				</div>
			</div>
		</div>

	</div>
</section>

<section class="section services">
	<div class="container-fluid">
		<div class="services__title">
			<h2>
				Организуем мероприятие
				<span>с безупречным вкусом</span>
			</h2>
			<p>Страсть к высокой кухне и качественному сервису</p>
			<p>у нас в крови. Мы формируем мобильное меню</p>
			<p>и дизайн-сервировку с учетом локации и стиля события</p>
		</div>
		<div class="services__list">
			<div class="services__col">
				<a href="" class="services__item lazy" data-bg="./assets/images/services_img.jpg">
					<h3>
						Выездной кейтеринг
					</h3>

					<p>
						Свадьбы, конференции, корпоративы, тимбилдинги, детские праздники по четкой программе с таймингом.
					</p>
					<div class="icon-wrap">
						<svg class="services__item-icon">
							<use xlink:href="./assets/images/sprite.svg#long-arrow"></use>
						</svg>
					</div>
				</a>
			</div>
			<div class="services__col">
				<a href="" class="services__item lazy" data-bg="./assets/images/services_img2.jpg">
					<h3>
						Мероприятия под ключ
					</h3>

					<p>
						Свадьбы, конференции, корпоративы, тимбилдинги, детские праздники по четкой программе с таймингом.
					</p>
					<div class="icon-wrap">
						<svg class="services__item-icon">
							<use xlink:href="./assets/images/sprite.svg#long-arrow"></use>
						</svg>
					</div>
				</a>
			</div>
			<div class="services__col">
				<a href="" class="services__item lazy" data-bg="./assets/images/services_img3.jpg">
					<h3>
						Корпоративное питание
					</h3>

					<p>
						Свадьбы, конференции, корпоративы, тимбилдинги, детские праздники по четкой программе с таймингом.
					</p>
					<div class="icon-wrap">
						<svg class="services__item-icon">
							<use xlink:href="./assets/images/sprite.svg#long-arrow"></use>
						</svg>
					</div>
				</a>
			</div>
			<div class="services__col">
				<a href="" class="services__item lazy" data-bg="./assets/images/services_img4.jpg">
					<h3>
						База отдыха  «Пасторское озеро»
					</h3>

					<p class="services__item-text">
						Свадьбы, конференции, корпоративы, тимбилдинги, детские праздники по четкой программе с таймингом.
					</p>
					<div class="icon-wrap">
						<svg class="services__item-icon">
							<use xlink:href="./assets/images/sprite.svg#long-arrow"></use>
						</svg>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="section awards lazy" data-bg="./assets/images/awards_bg.jpg">
	<div class="awards__container">

		<div class="awards__container-border top">
			<img src="./assets/images/icomoon/border-top-gold.svg" alt="">
		</div>
		<div class="awards__container-border bottom">
			<img src="./assets/images/icomoon/border-bottom-white.svg" alt="">
		</div>

		<h2>
			С вас <span>– четкий запрос. <br> С нас – креативное решение</span>
		</h2>

		<p>
			Мы уже воплотили в жизнь невероятные идеи
			для <strong>«Сбербанка», «Мегафона» и «Ростелекома»</strong>
		</p>
		<p>
			<strong>Награждены Почетной грамотой президента <br> РФ</strong> за организацию Международного Экономического Форума
		</p>

		<div class="awards__icons">
			<img src="./assets/images/sber.png" alt="Логотип сбербанка">
			<img src="./assets/images/megafon.png" alt="Логотип мегафона">
			<img src="./assets/images/rostelecom.png" alt="Логотип Ростелекома">
		</div>
	</div>
</section>

<section class="section our-cases">
	<div class="our-cases__container">
		<div class="our-cases__dots custom-dots"></div>
		<div class="our-cases__arrows custom-arrows"></div>

		<div class="our-cases__slider">
			<div class="our-cases__item cases_item">
				<div class="cases_item__inner">
					<div class="cases_item__img">
						<img class="lazy" data-src="./assets/images/cases__item-img.jpg" alt="Губернаторский прием на 2 600 человек">
					</div>
					<div class="cases_item__info">
						<h2>
							Губернаторский прием <span>на 2 600 человек</span>
						</h2>
						<div class="cases_item__info_location">
							<p>
								г. Санкт-Петербург
							</p>
							<p>
								3-4 декабря 2013 г.
							</p>
						</div>
						<div class="cases_item__info_text">
							<p>
								Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума.
							</p>
						</div>
						<button class="show_more">
							<span class="show_text">Читать дальше</span>
							<span class="close_text">Скрыть</span>
						</button>


					</div>
				</div>
			</div>
			<div class="our-cases__item cases_item">
				<div class="cases_item__inner">
					<div class="cases_item__img">
						<img class="lazy" data-src="./assets/images/cases__item-img.jpg" alt="Губернаторский прием на 2 600 человек">
					</div>
					<div class="cases_item__info">
						<h2>
							Губернаторский прием <span>на 2 600 человек</span>
						</h2>
						<div class="cases_item__info_location">
							<p>
								г. Санкт-Петербург
							</p>
							<p>
								3-4 декабря 2013 г.
							</p>
						</div>
						<div class="cases_item__info_text">
							<p>
								Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума.
							</p>
						</div>
						<button class="show_more">
							<span class="show_text">Читать дальше</span>
							<span class="close_text">Скрыть</span>
						</button>

					</div>
				</div>
			</div>
			<div class="our-cases__item cases_item">
				<div class="cases_item__inner">
					<div class="cases_item__img">
						<img class="lazy" data-src="./assets/images/cases__item-img.jpg" alt="Губернаторский прием на 2 600 человек">
					</div>
					<div class="cases_item__info">
						<h2>
							Губернаторский прием <span>на 2 600 человек</span>
						</h2>
						<div class="cases_item__info_location">
							<p>
								г. Санкт-Петербург
							</p>
							<p>
								3-4 декабря 2013 г.
							</p>
						</div>
						<div class="cases_item__info_text">
							<p>
								Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума Организовали губернаторский прием в рамках ежегодного Санкт-Петербургского Международного культурного форума.
							</p>
						</div>
						<button class="show_more">
							<span class="show_text">Читать дальше</span>
							<span class="close_text">Скрыть</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="our-cases__gallery">
		<a data-fancybox href="./assets/images/gallery1.jpg"><img src="./assets/images/gallery1.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery2.jpg"><img src="./assets/images/gallery2.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery3.jpg"><img src="./assets/images/gallery3.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery4.jpg"><img src="./assets/images/gallery4.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery5.jpg"><img src="./assets/images/gallery5.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery6.jpg"><img src="./assets/images/gallery6.jpg" alt=""></a>
		<a data-fancybox href="./assets/images/gallery1.jpg"><img src="./assets/images/gallery1.jpg" alt=""></a>
	</div>
</section>

<section class="section our_team">
	<div class="our_team__title">
		<h2>
			Люди <span>имеют значение</span>
		</h2>
		<p>
			Наши еvent-герои превращают любое событие
			в бомбу с вау-эффектом.
		</p>
	</div>
	<div class="container-fluid">
		<div class="our_team__arrows custom-arrows"></div>

		<div class="our_team__container">
			<div class="our_team__item">
				<div class="our_team__item-image">
					<img class="lazy" data-src="./assets/images/team_image.jpg" alt="Член команды">
				</div>
				<div class="our_team__item-name">
					<p>Андрей Иванов</p>
					<p>Бренд-шеф</p>
				</div>

			</div>
			<div class="our_team__item">
				<div class="our_team__item-image">
					<img class="lazy" data-src="./assets/images/team_image1.jpg" alt="Член команды">
				</div>
				<div class="our_team__item-name">
					<p>Ольга Петрова</p>
					<p>Мать еvent-проектов</p>
				</div>

			</div>
			<div class="our_team__item">
				<div class="our_team__item-image">
					<img class="lazy" data-src="./assets/images/team_image2.jpg" alt="Член команды">
				</div>
				<div class="our_team__item-name">
					<p>Александр Федоров</p>
					<p>Король банкетов</p>
				</div>

			</div>
			<div class="our_team__item">
				<div class="our_team__item-image">
					<img class="lazy" data-src="./assets/images/team_image2.jpg" alt="Член команды">
				</div>
				<div class="our_team__item-name">
					<p>Андрей Иванов</p>
					<p>Бренд-шеф</p>
				</div>

			</div>
			<div class="our_team__item">
				<div class="our_team__item-image">
					<img class="lazy" data-src="./assets/images/team_image2.jpg" alt="Член команды">
				</div>
				<div class="our_team__item-name">
					<p>Андрей Иванов</p>
					<p>Бренд-шеф</p>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="section footer_form">
	<div class="footer_form__image">
		<picture>
			<source data-srcset="./assets/images/webp/footer_img.webp" type="image/webp">
			<source data-srcset="./assets/images/footer_img.png" type="image/png">
			<img class="lazy" data-src="./assets/images/footer_img.png" alt="Официанты">
		</picture>

	</div>
	<div class="container-fluid">
		<div class="footer_form__title">
			<h2>
				Получите <span>расчет стоимости
				и концепцию проекта</span> бесплатно
			</h2>
		</div>

		<div class="footer_form__container">
			<form class="validate">
				<div class="input__row">
					<div class="input__col">
						<div class="input_container">
							<i class="plus" ></i>
							<input type="text" data-mask="letter" placeholder="Какое мероприятие надо организовать?" name="event">
						</div>
						<div class="input_container user-icon">
							<input type="text" data-mask="numbers" placeholder="На сколько человек?" name="numberOfPeople">
						</div>
						<div class="input_container calendar-icon">
							<input class="required" type="text" data-mask="calendar" placeholder="На какую дату?" name="date">
						</div>
					</div>
					<div class="input__col">
						<div class="input_container">
							<input type="text" placeholder="Ваше имя">
						</div>
						<div class="input_container">
							<input type="text" placeholder="Телефон*">
						</div>
						<div class="input_container">
							<input type="text" placeholder="Email">
						</div>
					</div>
				</div>

				<div class="button__wrap">
					<button class="btn" >
						<span>Получить программу и расчет стоимости</span>
					</button>
					<div class="agreement-check lt animate-top">
						<input class="agreement" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
						<label class="agreement-label">
						<span class="check"></span>Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
						</label>
					</div>
				</div>

			</form>
		</div>
	</div>
</section>



<? include $_SERVER['DOCUMENT_ROOT'].'/app/footer.php' ?>
