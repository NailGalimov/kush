</main>

<footer class="footer_custom">
    <div class="container-fluid">
        <div class="footer_custom__inner footer_row">
            <div class="footer_col">
                <a href="#" class="main-logo">
                    <img src="./assets/images/logo.png" alt="Логотип">
                    <p>Event-агентство полного цикла</p>
                </a>

                <div class="footer_copyright">
                    ⓒ Все права защищены. 
                    Информация на сайте не является 
                    публичной офертой. ИНН: 00000000000
                </div>
            </div>
            <div class="footer_col">
                <nav class="footer_nav">
                    <div class="footer_nav__col">
                        <ul>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Проекты</a></li>
                            <li><a href="">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="footer_nav__col">
                        <ul>
                            <li>
                                <ul>
                                    <li><a href="#">Кейтеринг</a></li>
                                    <li>
                                        Организация мероприятий:
                                        <ul>
                                            <li><a href="#">Романтические вечера</a></li>
                                            <li><a href="#">Свадьба под ключ</a></li>
                                            <li><a href="#">Организация тимбилдинга</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Корпоративное питание</a></li>
                                    <li><a href="#">Event-площадка «Пасторское озеро»</a></li>
                                    <li><a href="#">Кондитерская</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>

                </nav>
            
            </div>
            <div class="footer_col">
                <div class="main-location">
					<p>
						г. Санкт-Петербург
						Ленинградская область
					</p>
				</div>

				<div class="main-links">
					<a data-text="facebook" class="main-links__icon" href="#"><span class="icon-fb"></span></a>
					<a data-text="instagram" class="main-links__icon" href="#"><span class="icon-inst"></span></a>
					<a data-text="YouTube" class="main-links__icon" href="#"><span class="icon-triangle"></span></a>
				</div>
            </div>

            <div class="footer_col">
                <div class="main-tel">
					<a href="tel:+78129211152">+ 7 (812) 92-111-52</a>
				</div>

                <button class="footer_btn_order_call" >
					<span>Заказать звонок</span>
				</button>

                <div class="mini-copyright">
                    ⓒ kushavel, 2010 – 2021
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="assets/js/libs.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
