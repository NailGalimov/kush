const { src, dest, watch, parallel, series } = require('gulp');
const scss = require('gulp-sass');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify-es').default;
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const imagemin = require('gulp-imagemin');
const del = require('del');
const gcmq = require('gulp-group-css-media-queries');


function browsersync() {
    browserSync.init({
        // server: {
        //     baseDir: 'app/'
        // },
        notify: false,
		proxy: "kush/app",
		// online: false, // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    });
}

function cleanDist() {
    return del('dist')
}

function libs() {
    return src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/slick-carousel/slick/slick.js',
        'node_modules/gsap/dist/gsap.js',
        'node_modules/gsap/dist/ScrollTrigger.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
		'node_modules/formatfield/all.min.js',
        // 'node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
        // 'node_modules/perfect-scrollbar/dist/perfect-scrollbar.js',
        'node_modules/vanilla-lazyload/dist/lazyload.js',
        // 'node_modules/body-scroll-lock/lib/bodyScrollLock.js',
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(dest('app/assets/js'))
    .pipe(browserSync.stream())
}

function stylelibs() {
    return src([
        'node_modules/slick-carousel/slick/slick.css',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css',
        // 'node_modules/perfect-scrollbar/css/perfect-scrollbar.css',
    ])
    .pipe(concat('libs.style.css'))
    .pipe(cssmin())
    .pipe(dest('app/assets/css'))
    .pipe(browserSync.stream())
}


function styles() {
    return src('app/assets/scss/style.scss')
        .pipe(scss({outputStyle: 'expanded'}))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 version'],
            grid: "autoplace",
        }))
        .pipe(gcmq())
        .pipe(dest('app/assets/css'))
        .pipe(browserSync.stream())
};

function images() {
    return src('app/assets/images/**/*')
    .pipe(imagemin(
        [
            imagemin.gifsicle({interlaced:true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    { removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })

        ]
    ))
    .pipe(dest('dist/images'))
}

function build() {
    return src([
        'app/assets/css/libs.style.css',
        'app/assets/css/style.css',
        'app/assets/fonts/**.*',
        'app/assets/js/libs.min.js',
        'app/assets/js/main.js',
        'app/*.html',
    ], {base: 'app'})
    .pipe(dest('dist'))
}

function watching() {
    watch(['app/assets/scss/**/*.scss'], styles);
    watch(['app/assets/js/**/*.js', '!app/assets/js/libs.min.js'], libs);
    watch(['app/assets/css/**/*.css', '!app/assets/css/libs.style.css'], stylelibs);
    watch(['app/*.html', 'app/*.php']).on('change', browserSync.reload);
}

exports.styles = styles;
exports.stylelibs = stylelibs;
exports.watching = watching;
exports.browsersync = browsersync;
exports.libs = libs;
exports.images = images;
exports.cleanDist = cleanDist;


exports.build = series(cleanDist, images, build);
exports.default = parallel(styles, libs, stylelibs, browsersync, watching);
